﻿using AutoMapper;
using Client.Common.DTO;
using Client.Common.Entities;

namespace Client.BLL.Services.Abstract
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Team, TeamDTO>();
            CreateMap<TeamDTO, Team>();

            CreateMap<User, UserDTO>();
            CreateMap<UserDTO, User>();

            CreateMap<Project, ProjectDTO>();
            CreateMap<ProjectDTO, Project>();

            CreateMap<Client.Common.Entities.Task, TaskDTO>();
            CreateMap<TaskDTO, Client.Common.Entities.Task>();

            CreateMap<UserSummary, UserSummaryDTO>();
            CreateMap<UserSummaryDTO, UserSummary>();


            CreateMap<ProjectSummary, ProjectSummaryDTO>();
            CreateMap<ProjectSummaryDTO, ProjectSummary>();
        }
    }
}
