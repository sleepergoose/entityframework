﻿using Client.Common.DTO;
using System.Threading.Tasks;
using Client.BLL.Services.Abstract;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Client.Common.Entities;

namespace Client.BLL.Services
{
    public class UserService : BaseService
    {
        private string _url;

        public UserService(string host) : base(host)
        {
            _url = $"{Host}/api/Users";
        }


        public async Task<IEnumerable<User>> GetAllUsers()
        {
            var response = await _httpService.GetAsync(_url);

            var users = JsonConvert.DeserializeObject<ICollection<UserDTO>>(response);

            return _mapper.Map<IEnumerable<User>>(users);
        }


        public async Task<User> GetUser(int id)
        {
            var response = await _httpService.GetAsync($"{_url}/{id}");

            var user = JsonConvert.DeserializeObject<UserDTO>(response);

            return _mapper.Map<User>(user);
        }
    }
}
