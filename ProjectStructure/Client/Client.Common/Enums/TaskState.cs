﻿namespace Lecture1.Common.Enums
{
    public enum TaskState
    {
        Pending = 0,
        Canceled,
        Unfinished,
        Finished
    }
}
