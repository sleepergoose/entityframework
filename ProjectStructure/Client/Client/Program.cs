﻿using System;
using Client.Data;
using Client.BLL.Services;
using Microsoft.Extensions.Configuration;
using Client.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Client
{
    class Program
    {
        private static ServiceProvider _services;

        private static IConfiguration AppConfiguration { get; set; }


        static void Main(string[] args)
        {
            // Settings and IoC
            Configure();


            IView view = _services.GetService<IView>();
            IQueries queries = _services.GetService<IQueries>();

            Presenter presenter = new Presenter(view, queries);
            presenter.Run();

            Console.WriteLine("");
        }

        private static void Configure()
        {
            // Getting server host address from appsettings.json
            var builder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            AppConfiguration = builder.Build();

            var host = AppConfiguration.GetSection("host").Value;


            // IoC
            _services = new ServiceCollection()
                .AddScoped<HttpService>()

                .AddScoped(_ => new ProjectService(host))
                .AddScoped(_ => new UserService(host))
                .AddScoped(_ => new TeamService(host))
                .AddScoped(_ => new TaskService(host))
                .AddScoped(_ => new LinqService(host))

                .AddScoped<IDataLoader, DataLoader>()
                //.AddScoped<IQueries, LocalQueries>() // Local queries as in the first task  
                .AddScoped<IQueries, RemoteQueries>() // Queries to the remote server

                .AddSingleton<IView, View>()
                .BuildServiceProvider();
        }
    }
}
