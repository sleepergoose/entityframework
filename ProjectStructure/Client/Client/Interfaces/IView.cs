﻿using Client.Common.Entities;
using System.Collections.Generic;


namespace Client.Interfaces
{
    public interface IView
    {
        void ShowMenu();

        void WriteTextToCenter(string text);

        void ShowTaskAmount(Dictionary<Project, int> dict);

        void ShowTaskList(List<Client.Common.Entities.Task> tasks);

        void ShowFinishedTasks(List<(int Id, string Name)> list);

        void ShowTeamsMembers(List<(int Id, string TeamName, List<User> Users)> teams);

        void ShowUsersWithTasks(List<User> users);

        void ShowUserSummary(UserSummary userSummary);

        void ShowProjectSummary(List<ProjectSummary> projectSummaries);

        void WriteErrorToCenter(string error);

        string GetTextResponse(string message);
    }
}
