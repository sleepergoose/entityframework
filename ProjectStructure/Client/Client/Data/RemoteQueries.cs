﻿using System.Collections.Generic;
using Client.BLL.Services;
using Client.Common.Entities;
using Client.Interfaces;


namespace Client.Data
{
    public sealed class RemoteQueries : IQueries
    {

        private readonly LinqService _linqService;

        public RemoteQueries(LinqService linqService)
        {
            _linqService = linqService;
        }


        /* -- 1 -- */
        public Dictionary<Project, int> GetTasksAmount(int authorId)
        {
            return _linqService.GetTasksAmount(authorId).Result;
        }


        /* -- 2 -- */
        public List<Client.Common.Entities.Task> GetTasksList(int performerId)
        {
            return _linqService.GetTasksList(performerId).Result;
        }


        /* -- 3 -- */
        public List<(int Id, string Name)> GetFinishedTasks(int performerId)
        {
            return _linqService.GetFinishedTasks(performerId).Result;
        }


        /* -- 4 -- */
        public List<(int Id, string TeamName, List<User> Users)> GetTeamsMembers()
        {
            return _linqService.GetTeamsMembers().Result;
        }


        /* -- 5 -- */
        public List<User> GetUsersWithTasks()
        {
            return _linqService.GetUsersWithTasks().Result;
        }


        /* -- 6 -- */
        public UserSummary GetUserSummary(int userId)
        {
            return _linqService.GetUserSummary(userId).Result;
        }


        /* -- 7 -- */
        public List<ProjectSummary> GetProjectSummary()
        {
            return _linqService.GetProjectSummary().Result;
        }
    }
}
