﻿using AutoMapper;
using System.Threading.Tasks;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.DAL.Context;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.Services.Abstract;

namespace ProjectStructure.BLL.Services
{
    public sealed class ProjectService : BaseService
    {

        public ProjectService(ApplicationContext context, IMapper mapper)
            : base(context, mapper)
        { }


        public async Task<IEnumerable<ProjectDTO>> GetProjects()
        {
            var projects = await _context.Projects.AsNoTracking().ToListAsync();

            return _mapper.Map<IEnumerable<ProjectDTO>>(projects);
        }


        public async Task<ProjectDTO> GetProject(int id)
        {
            var project = await _context.Projects.FirstOrDefaultAsync(p => p.Id == id);

            return _mapper.Map<ProjectDTO>(project);
        }


        public async Task<ProjectDTO> AddProject(ProjectDTO projectDTO)
        {
            var project = _mapper.Map<DAL.Entities.Project>(projectDTO);

            _context.Projects.Add(project);

            await _context.SaveChangesAsync();
            
            var createdProject = await _context.Projects.FirstOrDefaultAsync(p => p.Id == project.Id);

            return _mapper.Map<ProjectDTO>(createdProject);
        }


        public async Task<ProjectDTO> UpdateProject(ProjectDTO projectDTO)
        {
            var project = _mapper.Map<DAL.Entities.Project>(projectDTO);

            _context.Projects.Update(project);

            await _context.SaveChangesAsync();

            var updatedProject = await _context.Projects.FirstOrDefaultAsync(p => p.Id == project.Id);

            return _mapper.Map<ProjectDTO>(updatedProject);
        }


        public async Task<int> DeleteProject(int id)
        {
            var deletedProject = await _context.Projects.FirstOrDefaultAsync(p => p.Id == id);
            
            _context.Projects.Remove(deletedProject);

            await _context.SaveChangesAsync();

            return id;
        }
    }
}
