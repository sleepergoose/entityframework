﻿using AutoMapper;
using System.Threading.Tasks;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.DAL.Context;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.Services.Abstract;

namespace ProjectStructure.BLL.Services
{
    public sealed class TeamService : BaseService
    {
        public TeamService(ApplicationContext context, IMapper mapper)
            : base(context, mapper)
        { }


        public async Task<IEnumerable<TeamDTO>> GetTeams()
        {
            var teams = await _context.Teams.AsNoTracking().ToListAsync();

            return _mapper.Map<IEnumerable<TeamDTO>>(teams);
        }


        public async Task<TeamDTO> GetTeam(int id)
        {
            var team = await _context.Teams.FirstOrDefaultAsync(p => p.Id == id);

            return _mapper.Map<TeamDTO>(team);
        }


        public async Task<TeamDTO> AddTeam(TeamDTO teamDTO)
        {
            var team = _mapper.Map<DAL.Entities.Team>(teamDTO);

            _context.Teams.Add(team);

            await _context.SaveChangesAsync();
            
            var createdTeam = await _context.Teams.FirstOrDefaultAsync(t => t.Id == team.Id);

            return _mapper.Map<TeamDTO>(createdTeam);
        }


        public async Task<TeamDTO> UpdateTeam(TeamDTO teamDTO)
        {
            var team = _mapper.Map<DAL.Entities.Team>(teamDTO);

            _context.Teams.Update(team);

            await _context.SaveChangesAsync();

            var updatedTeam = await _context.Teams.FirstOrDefaultAsync(t => t.Id == team.Id);

            return _mapper.Map<TeamDTO>(updatedTeam);
        }


        public async Task<int> DeleteTeam(int id)
        {
            var deletedTeam = await _context.Teams.FirstOrDefaultAsync(p => p.Id == id);

            _context.Teams.Remove(deletedTeam);

            await _context.SaveChangesAsync();

            return id;
        }
    }
}
