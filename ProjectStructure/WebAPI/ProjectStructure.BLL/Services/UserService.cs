﻿using AutoMapper;
using System.Threading.Tasks;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.DAL.Context;
using Microsoft.EntityFrameworkCore;

using ProjectStructure.BLL.Services.Abstract;

namespace ProjectStructure.BLL.Services
{
    public sealed class UserService : BaseService
    {
        public UserService(ApplicationContext context, IMapper mapper)
            : base(context, mapper)
        { }


        public async Task<IEnumerable<UserDTO>> GetUsers()
        {
            var users = await _context.Users.AsNoTracking().ToListAsync();

            return _mapper.Map<IEnumerable<UserDTO>>(users);
        }


        public async Task<UserDTO> GetUser(int id)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == id);

            return _mapper.Map<UserDTO>(user);
        }


        public async Task<UserDTO> AddUser(UserDTO userDTO)
        {
            var user = _mapper.Map<DAL.Entities.User>(userDTO);

            _context.Users.Add(user);

            await _context.SaveChangesAsync();

            var createdUser = await _context.Users.FirstOrDefaultAsync(u => u.Id == user.Id);

            return _mapper.Map<UserDTO>(createdUser);
        }


        public async Task<UserDTO> UpdateUser(UserDTO userDTO)
        {
            var user = _mapper.Map<DAL.Entities.User>(userDTO);

            _context.Users.Update(user);

            await _context.SaveChangesAsync();

            var updatedUser = await _context.Users.FirstOrDefaultAsync(u => u.Id == user.Id);

            return _mapper.Map<UserDTO>(updatedUser);
        }


        public async Task<int> DeleteUser(int id)
        {
            var deletedUser = await _context.Users.FirstOrDefaultAsync(u => u.Id == id);

            _context.Users.Remove(deletedUser);

            await _context.SaveChangesAsync();

            return id;
        }
    }
}
