﻿using System;
using AutoMapper;
using System.Linq;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.DAL.Context;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.Services.Abstract;

namespace ProjectStructure.BLL.Services
{
    public sealed class LinqService : BaseService
    {
        public LinqService(ApplicationContext context, IMapper mapper)
            : base(context, mapper)
        { }


        /* -- 1 -- */
        public Dictionary<ProjectDTO, int> GetTasksAmount(int authorId)
        {
            var result = _context.Projects.Where(project => project.AuthorId == authorId)
                .Include(p => p.Tasks)
                .AsNoTracking()
                .ToDictionary(key => key, value => (value.Tasks == null ? 0 : value.Tasks.Count()));
            
            return _mapper.Map<Dictionary<ProjectDTO, int>>(result);
        }



        /* -- 2 -- */
        public List<TaskDTO> GetTasksList(int performerId)
        {
            var result = _context.Tasks.Where(task => task.PerformerId == performerId
                            && task.Name.Length < 45).AsNoTracking().ToList();

            return _mapper.Map<List<TaskDTO>>(result);
        }



        /* -- 3 -- */
        public List<(int Id, string Name)> GetFinishedTasks(int performerId)
        {
            return _context.Tasks.Where(task =>
                            task.PerformerId == performerId &&
                            task.FinishedAt != null &&
                            task.FinishedAt.Value.Year == DateTime.Now.Year)
                .AsNoTracking().AsEnumerable()
                .Select(task => (id: task.Id, name: task.Name))
                .ToList();              
        }


        /* -- 4 -- */
        public List<(int Id, string TeamName, List<UserDTO> Users)> GetTeamsMembers()
        {
            return _context.Teams
                .Include(team => team.Members)
                .Where(team => team.Members.All(m => m.BirthDay.Year < DateTime.Now.Year - 10))
                .AsNoTracking()
                .AsEnumerable()
                .Select(team => (
                       Id: team.Id,
                       TeamName: team.Name,
                       Users: _mapper.Map<List<UserDTO>>(team.Members
                                    .OrderByDescending(performer => performer.RegisteredAt).ToList())                
                )).ToList();
        }



        /* -- 5 -- */
        public List<UserWithTasksDTO> GetUsersWithTasks()
        {
            return _context.Users
                .Include(user => user.Tasks)
                .OrderBy(user => user.FirstName)
                .AsNoTracking()
                .AsEnumerable()
                .Select(user => new UserWithTasksDTO { 
                    User = _mapper.Map<UserDTO>(user),
                    Tasks = _mapper.Map<List<TaskDTO>>(user.Tasks.OrderByDescending(t => t.Name.Length).ToList())
                })
                .ToList();
        }


        /* -- 6 -- */
        public UserSummaryDTO GetUserSummary(int userId)
        {
            return _context.Projects.Where(p => p.AuthorId == userId)
                .Include(p => p.Author)
                    .ThenInclude(user => user.Tasks)
                .Include(p => p.Tasks)
                .OrderByDescending(p => p.CreatedAt)
                .Take(1)
                .AsNoTracking()
                .AsEnumerable()
                .Select(project => new UserSummaryDTO
                {
                    User = _mapper.Map<UserDTO>(project.Author),

                    LastProject = _mapper.Map<ProjectDTO>(project),

                    LastProjectTasksAmount = project.Tasks == null ? 0 : project.Tasks.Count(),

                    BadTasksAmount = project.Author.Tasks == null ? 0 : project.Author.Tasks.Where(task => task.FinishedAt == null).Count(),

                    LongestTask = _mapper.Map<TaskDTO>(project.Author.Tasks
                            .OrderByDescending(t => (t.FinishedAt == null ? DateTime.Now : t.FinishedAt) - t.CreatedAt)
                            .FirstOrDefault())
                })
                .FirstOrDefault();
        }


        /* -- 7 -- */
        public List<ProjectSummaryDTO> GetProjectSummary()
        {
            return _context.Projects
                .Include(p => p.Tasks)
                .Include(p => p.Team)
                    .ThenInclude(team => team.Members)
                .AsNoTracking()
                .AsEnumerable()
                .Select(project => new ProjectSummaryDTO {

                    Project = _mapper.Map<ProjectDTO>(project),

                    LongestTaskByDescription = _mapper.Map<TaskDTO>(project.Tasks?.OrderByDescending(task => task.Description.Length)
                                                    .FirstOrDefault()),

                    ShortestTaskByName = _mapper.Map<TaskDTO>(project.Tasks?.OrderBy(task => task.Name.Length).FirstOrDefault()),

                    TeamMemberAmount = new Func<int>(() => { 
                            if (project.Description.Length > 20 || (project.Tasks != null && project.Tasks.Count() < 3))
                            {
                                return project.Team == null ? 0 : project.Team.Members.Count();
                            }    
                            return 0;
                        }).Invoke() /* complex ternary operator - it's bad */

                }).ToList();       
        }
    }
}
