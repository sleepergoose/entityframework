﻿using AutoMapper;
using System.Threading.Tasks;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.DAL.Context;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.Services.Abstract;

namespace ProjectStructure.BLL.Services
{
    public sealed class TaskService : BaseService
    {
        public TaskService(ApplicationContext context, IMapper mapper)
            : base(context, mapper)
        { }


        public async Task<IEnumerable<TaskDTO>> GetTasks()
        {
            var tasks = await _context.Tasks.AsNoTracking().ToListAsync();

            return _mapper.Map<IEnumerable<TaskDTO>>(tasks);
        }


        public async Task<TaskDTO> GetTask(int id)
        {
            var task = await _context.Tasks.FirstOrDefaultAsync(p => p.Id == id);

            return _mapper.Map<TaskDTO>(task);
        }


        public async Task<TaskDTO> AddTask(TaskDTO taskDTO)
        {
            var task = _mapper.Map<DAL.Entities.Task>(taskDTO);

            _context.Tasks.Add(task);

            await _context.SaveChangesAsync();
            
            var createdTask = await _context.Tasks.FirstOrDefaultAsync(p => p.Id == task.Id);

            return _mapper.Map<TaskDTO>(createdTask);
        }


        public async Task<TaskDTO> UpdateTask(TaskDTO taskDTO)
        {
            var task = _mapper.Map<DAL.Entities.Task>(taskDTO);

            _context.Tasks.Update(task);

            await _context.SaveChangesAsync();

            var updatedTask = await _context.Tasks.FirstOrDefaultAsync(p => p.Id == task.Id);

            return _mapper.Map<TaskDTO>(updatedTask);
        }


        public async Task<int> DeleteTask(int id)
        {
            var deletedTask = await _context.Tasks.FirstOrDefaultAsync(p => p.Id == id);

            _context.Tasks.Remove(deletedTask);

            await _context.SaveChangesAsync();

            return id;
        }
    }
}
