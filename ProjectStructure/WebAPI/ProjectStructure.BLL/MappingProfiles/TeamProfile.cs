﻿using AutoMapper;
using ProjectStructure.BLL.DTO;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.BLL.MappingProfiles
{
    public sealed class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDTO>();

            CreateMap<TeamDTO, Team>();
        }
    }
}
