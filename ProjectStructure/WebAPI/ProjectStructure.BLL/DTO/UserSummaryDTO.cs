﻿using ProjectStructure.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.DTO
{
    public sealed class UserSummaryDTO
    {
        public UserDTO User { get; set; }
        public ProjectDTO LastProject { get; set; }
        public int LastProjectTasksAmount { get; set; }
        public int BadTasksAmount { get; set; }
        public TaskDTO LongestTask { get; set; }
    }
}
