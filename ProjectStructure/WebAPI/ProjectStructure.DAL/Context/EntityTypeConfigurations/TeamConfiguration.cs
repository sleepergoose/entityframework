﻿using ProjectStructure.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ProjectStructure.DAL.Context.EntityTypeConfigurations
{
    public sealed class TeamConfiguration : IEntityTypeConfiguration<Team>
    {
        public void Configure(EntityTypeBuilder<Team> builder)
        {
            builder.Property(p => p.Name).HasColumnName("TeamName");

            builder.Property(p => p.CreatedAt)
                .IsRequired()
                .HasColumnType("datetime2(2)")
                .HasDefaultValueSql("GETDATE()");
        }
    }
}
