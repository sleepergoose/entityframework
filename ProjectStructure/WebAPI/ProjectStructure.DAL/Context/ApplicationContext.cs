﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.DAL.Context
{
    public class ApplicationContext : DbContext
    {

        public DbSet<Project> Projects { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Task> Tasks { get; set; }


        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            :base(options)
        { }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Configure(); // Fluent API

            modelBuilder.Seed();
        }
    }
}
