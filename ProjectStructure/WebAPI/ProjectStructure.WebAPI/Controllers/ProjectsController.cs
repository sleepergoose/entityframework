﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.BLL.Services;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly ProjectService _projectService;


        public ProjectsController(ProjectService projectService)
        {
            _projectService = projectService;
        }


        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProjectDTO>>> Get()
        {
            return Ok(await _projectService.GetProjects());
        }


        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDTO>> Get(int id)
        {
            return Ok(await _projectService.GetProject(id));
        }


        [HttpPost]
        public async Task<ActionResult> Post([FromBody] ProjectDTO projectDTO)
        {
            return Ok(await _projectService.AddProject(projectDTO));
        }


        [HttpPut]
        public async Task<ActionResult> Put([FromBody] ProjectDTO projectDTO)
        {
            return Ok(await _projectService.UpdateProject(projectDTO));
        }


        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            return Ok(await _projectService.DeleteProject(id));
        }
    }
}
