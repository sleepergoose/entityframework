﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.BLL.Services;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly TeamService _teamService;


        public TeamsController(TeamService teamService)
        {
            _teamService = teamService;
        }


        [HttpGet]
        public async Task<ActionResult<IEnumerable<TeamDTO>>> Get()
        {
            return Ok(await _teamService.GetTeams());
        }


        [HttpGet("{id}")]
        public async Task<ActionResult<TeamDTO>> Get(int id)
        {
            return Ok(await _teamService.GetTeam(id));
        }


        [HttpPost]
        public async Task<ActionResult> Post([FromBody] TeamDTO teamDTO)
        {
            return Ok(await _teamService.AddTeam(teamDTO));
        }


        [HttpPut]
        public async Task<ActionResult> Put([FromBody] TeamDTO teamDTO)
        {
            return Ok(await _teamService.UpdateTeam(teamDTO));
        }


        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
             return Ok(await _teamService.DeleteTeam(id));
        }
    }
}
