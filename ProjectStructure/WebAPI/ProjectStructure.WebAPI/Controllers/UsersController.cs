﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.BLL.Services;
using System.Threading.Tasks;


namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserService _userService;


        public UsersController(UserService userService)
        {
            _userService = userService;
        }


        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserDTO>>> Get()
        {
            return Ok(await _userService.GetUsers());
        }


        [HttpGet("{id}")]
        public async Task<ActionResult<UserDTO>> Get(int id)
        {
            return Ok(await _userService.GetUser(id));
        }


        [HttpPost]
        public async Task<ActionResult> Post([FromBody] UserDTO userDTO)
        {
            return Ok(await _userService.AddUser(userDTO));
        }


        [HttpPut]
        public async Task<ActionResult> Put([FromBody] UserDTO userDTO)
        {
            return Ok(await _userService.UpdateUser(userDTO));
        }


        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            return Ok(await _userService.DeleteUser(id));
        }
    }
}
