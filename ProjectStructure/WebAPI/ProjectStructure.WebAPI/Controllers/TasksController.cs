﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.BLL.Services;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {

        private readonly TaskService _taskService;


        public TasksController(TaskService taskService)
        {
            _taskService = taskService;
        }


        [HttpGet]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> Get()
        {
            return Ok(await _taskService.GetTasks());
        }


        [HttpGet("{id}")]
        public async Task<ActionResult<TaskDTO>> Get(int id)
        {
            return Ok(await _taskService.GetTask(id));
        }


        [HttpPost]
        public async Task<ActionResult> Post([FromBody] TaskDTO taskDTO)
        {
            return Ok(await _taskService.AddTask(taskDTO));
        }


        [HttpPut]
        public async Task<ActionResult> Put([FromBody] TaskDTO taskDTO)
        {
            return Ok(await _taskService.UpdateTask(taskDTO));
        }


        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            return Ok(await _taskService.DeleteTask(id));
        }
    }
}
